require "application_system_test_case"

class OrdersTest < ApplicationSystemTestCase
  setup do
    @order = orders(:one)
  end

  test "visiting the index" do
    visit orders_url
    assert_selector "h1", text: "Orders"
  end

  test "creating a Order" do
    visit orders_url
    click_on "New Order"

    fill_in "Contact", with: @order.contact_id
    fill_in "Customer kind", with: @order.customer_kind
    fill_in "Customer location", with: @order.customer_location
    fill_in "Delivery city", with: @order.delivery_city
    fill_in "Delivery country", with: @order.delivery_country
    fill_in "Delivery date at", with: @order.delivery_date_at
    fill_in "Delivery housenr", with: @order.delivery_housenr
    fill_in "Delivery name", with: @order.delivery_name
    fill_in "Delivery org", with: @order.delivery_org
    fill_in "Delivery plz", with: @order.delivery_plz
    fill_in "Delivery reference", with: @order.delivery_reference
    fill_in "Delivery status", with: @order.delivery_status
    fill_in "Delivery street", with: @order.delivery_street
    fill_in "Invoice city", with: @order.invoice_city
    fill_in "Invoice country", with: @order.invoice_country
    fill_in "Invoice date at", with: @order.invoice_date_at
    fill_in "Invoice housenr", with: @order.invoice_housenr
    fill_in "Invoice name", with: @order.invoice_name
    fill_in "Invoice org", with: @order.invoice_org
    fill_in "Invoice plz", with: @order.invoice_plz
    fill_in "Invoice reference", with: @order.invoice_reference
    fill_in "Invoice status", with: @order.invoice_status
    fill_in "Invoice street", with: @order.invoice_street
    check "Read privacy" if @order.read_privacy
    check "Read terms" if @order.read_terms
    fill_in "Status", with: @order.status
    fill_in "Token", with: @order.token
    click_on "Create Order"

    assert_text "Order was successfully created"
    click_on "Back"
  end

  test "updating a Order" do
    visit orders_url
    click_on "Edit", match: :first

    fill_in "Contact", with: @order.contact_id
    fill_in "Customer kind", with: @order.customer_kind
    fill_in "Customer location", with: @order.customer_location
    fill_in "Delivery city", with: @order.delivery_city
    fill_in "Delivery country", with: @order.delivery_country
    fill_in "Delivery date at", with: @order.delivery_date_at
    fill_in "Delivery housenr", with: @order.delivery_housenr
    fill_in "Delivery name", with: @order.delivery_name
    fill_in "Delivery org", with: @order.delivery_org
    fill_in "Delivery plz", with: @order.delivery_plz
    fill_in "Delivery reference", with: @order.delivery_reference
    fill_in "Delivery status", with: @order.delivery_status
    fill_in "Delivery street", with: @order.delivery_street
    fill_in "Invoice city", with: @order.invoice_city
    fill_in "Invoice country", with: @order.invoice_country
    fill_in "Invoice date at", with: @order.invoice_date_at
    fill_in "Invoice housenr", with: @order.invoice_housenr
    fill_in "Invoice name", with: @order.invoice_name
    fill_in "Invoice org", with: @order.invoice_org
    fill_in "Invoice plz", with: @order.invoice_plz
    fill_in "Invoice reference", with: @order.invoice_reference
    fill_in "Invoice status", with: @order.invoice_status
    fill_in "Invoice street", with: @order.invoice_street
    check "Read privacy" if @order.read_privacy
    check "Read terms" if @order.read_terms
    fill_in "Status", with: @order.status
    fill_in "Token", with: @order.token
    click_on "Update Order"

    assert_text "Order was successfully updated"
    click_on "Back"
  end

  test "destroying a Order" do
    visit orders_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Order was successfully destroyed"
  end
end
