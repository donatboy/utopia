require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order = orders(:one)
  end

  test "should get index" do
    get orders_url
    assert_response :success
  end

  test "should get new" do
    get new_order_url
    assert_response :success
  end

  test "should create order" do
    assert_difference('Order.count') do
      post orders_url, params: { order: { contact_id: @order.contact_id, customer_kind: @order.customer_kind, customer_location: @order.customer_location, delivery_city: @order.delivery_city, delivery_country: @order.delivery_country, delivery_date_at: @order.delivery_date_at, delivery_housenr: @order.delivery_housenr, delivery_name: @order.delivery_name, delivery_org: @order.delivery_org, delivery_plz: @order.delivery_plz, delivery_reference: @order.delivery_reference, delivery_status: @order.delivery_status, delivery_street: @order.delivery_street, invoice_city: @order.invoice_city, invoice_country: @order.invoice_country, invoice_date_at: @order.invoice_date_at, invoice_housenr: @order.invoice_housenr, invoice_name: @order.invoice_name, invoice_org: @order.invoice_org, invoice_plz: @order.invoice_plz, invoice_reference: @order.invoice_reference, invoice_status: @order.invoice_status, invoice_street: @order.invoice_street, read_privacy: @order.read_privacy, read_terms: @order.read_terms, status: @order.status, token: @order.token } }
    end

    assert_redirected_to order_url(Order.last)
  end

  test "should show order" do
    get order_url(@order)
    assert_response :success
  end

  test "should get edit" do
    get edit_order_url(@order)
    assert_response :success
  end

  test "should update order" do
    patch order_url(@order), params: { order: { contact_id: @order.contact_id, customer_kind: @order.customer_kind, customer_location: @order.customer_location, delivery_city: @order.delivery_city, delivery_country: @order.delivery_country, delivery_date_at: @order.delivery_date_at, delivery_housenr: @order.delivery_housenr, delivery_name: @order.delivery_name, delivery_org: @order.delivery_org, delivery_plz: @order.delivery_plz, delivery_reference: @order.delivery_reference, delivery_status: @order.delivery_status, delivery_street: @order.delivery_street, invoice_city: @order.invoice_city, invoice_country: @order.invoice_country, invoice_date_at: @order.invoice_date_at, invoice_housenr: @order.invoice_housenr, invoice_name: @order.invoice_name, invoice_org: @order.invoice_org, invoice_plz: @order.invoice_plz, invoice_reference: @order.invoice_reference, invoice_status: @order.invoice_status, invoice_street: @order.invoice_street, read_privacy: @order.read_privacy, read_terms: @order.read_terms, status: @order.status, token: @order.token } }
    assert_redirected_to order_url(@order)
  end

  test "should destroy order" do
    assert_difference('Order.count', -1) do
      delete order_url(@order)
    end

    assert_redirected_to orders_url
  end
end
