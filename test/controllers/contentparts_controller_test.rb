require 'test_helper'

class ContentpartsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contentpart = contentparts(:one)
  end

  test "should get index" do
    get contentparts_url
    assert_response :success
  end

  test "should get new" do
    get new_contentpart_url
    assert_response :success
  end

  test "should create contentpart" do
    assert_difference('Contentpart.count') do
      post contentparts_url, params: { contentpart: { container_id: @contentpart.container_id, container_type: @contentpart.container_type, content: @contentpart.content, kind: @contentpart.kind, position: @contentpart.position, status: @contentpart.status, token: @contentpart.token } }
    end

    assert_redirected_to contentpart_url(Contentpart.last)
  end

  test "should show contentpart" do
    get contentpart_url(@contentpart)
    assert_response :success
  end

  test "should get edit" do
    get edit_contentpart_url(@contentpart)
    assert_response :success
  end

  test "should update contentpart" do
    patch contentpart_url(@contentpart), params: { contentpart: { container_id: @contentpart.container_id, container_type: @contentpart.container_type, content: @contentpart.content, kind: @contentpart.kind, position: @contentpart.position, status: @contentpart.status, token: @contentpart.token } }
    assert_redirected_to contentpart_url(@contentpart)
  end

  test "should destroy contentpart" do
    assert_difference('Contentpart.count', -1) do
      delete contentpart_url(@contentpart)
    end

    assert_redirected_to contentparts_url
  end
end
