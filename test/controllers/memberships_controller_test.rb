require 'test_helper'

class MembershipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @membership = memberships(:one)
  end

  test "should get index" do
    get memberships_url
    assert_response :success
  end

  test "should get new" do
    get new_membership_url
    assert_response :success
  end

  test "should create membership" do
    assert_difference('Membership.count') do
      post memberships_url, params: { membership: { birth_on: @membership.birth_on, city: @membership.city, contact_id: @membership.contact_id, ends_at: @membership.ends_at, fee_amount: @membership.fee_amount, fee_mode: @membership.fee_mode, fee_period: @membership.fee_period, gender: @membership.gender, housenr: @membership.housenr, fee_check_at: @membership.fee_check_at, other_memberships: @membership.other_memberships, plz: @membership.plz, starts_at: @membership.starts_at, status: @membership.status, street: @membership.street, token: @membership.token, wants_membership: @membership.wants_membership } }
    end

    assert_redirected_to membership_url(Membership.last)
  end

  test "should show membership" do
    get membership_url(@membership)
    assert_response :success
  end

  test "should get edit" do
    get edit_membership_url(@membership)
    assert_response :success
  end

  test "should update membership" do
    patch membership_url(@membership), params: { membership: { birth_on: @membership.birth_on, city: @membership.city, contact_id: @membership.contact_id, ends_at: @membership.ends_at, fee_amount: @membership.fee_amount, fee_mode: @membership.fee_mode, fee_period: @membership.fee_period, gender: @membership.gender, housenr: @membership.housenr, fee_check_at: @membership.fee_check_at, other_memberships: @membership.other_memberships, plz: @membership.plz, starts_at: @membership.starts_at, status: @membership.status, street: @membership.street, token: @membership.token, wants_membership: @membership.wants_membership } }
    assert_redirected_to membership_url(@membership)
  end

  test "should destroy membership" do
    assert_difference('Membership.count', -1) do
      delete membership_url(@membership)
    end

    assert_redirected_to memberships_url
  end
end
