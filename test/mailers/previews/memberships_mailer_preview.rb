# Preview all emails at http://localhost:3000/rails/mailers/memberships_mailer
class MembershipsMailerPreview < ActionMailer::Preview
  
  # Preview this email at http://localhost:3000/rails/mailers/memberships_mailer/membership_accepted
  def membership_accepted
    MembershipsMailer.with( membership: Membership.last ).membership_accepted
  end
  
  # Preview this email at http://localhost:3000/rails/mailers/memberships_mailer/membership_accepted
  def payment_request
    MembershipsMailer.with( membership: Membership.last ).payment_request
  end
  
end
