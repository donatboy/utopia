# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ fundings.coffee'
  
  toggle_details = ->
    $ich = ($ this)
    console.log "-- toggle_details - #{$ich.attr 'id'} @ fundings.coffee"
    
    $ich.toggleClass 'collapsed'
  
  
  # --- Initialisierung ------------------------
  
  $main = ($ 'main')
  unless $main.data 'init_fundings'
    console.log '-- init'
    $main.on 'click.fundings',
        '.reward',
        toggle_details
    $main.data 'init_fundings', true
  

