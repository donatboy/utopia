# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ checkbox_show_hide.coffee'
  
  checkbox_switch = ->
    $ich = ($ this)
    console.log "-- checkbox_switch - ##{$ich.attr 'id'} @ checkbox_show_hide.coffee"
    sel = $ich.data 'sel'
    console.log "-- sel:#{sel}, val:#{$ich.val()}"
    if $ich.prop( 'checked' )
      ($ sel).slideDown 200
    else
      ($ sel).slideUp 200
    
    return false # hat_ziel
  
  
  # --- Initialisierung ------------------------
  
  $select = ($ 'input[data-action=checkbox_switch]')
  unless $select.data 'init_checkbox_show_hide'
    console.log '-- init init_checkbox_show_hide'
    $select.on 'change.init_checkbox_show_hide',
        '',
        checkbox_switch
    $select.data 'init_checkbox_show_hide', true
  

