class Owner < ApplicationRecord
  belongs_to :account
  belongs_to :funding
end
