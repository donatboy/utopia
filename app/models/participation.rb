class Participation < ApplicationRecord
  include Tokenize
  
  attr_accessor :participant_email
  
  belongs_to :entity, polymorphic: true
  belongs_to :participant, class_name: 'Contact'
  
  # --- INSTANCE METHODS -----------------------
  
  def secret
    id_hash [ participant_id, created_at ].join
  end
  
end
