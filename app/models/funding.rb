class Funding < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  belongs_to :account
  has_many :owners, dependent: :destroy
  has_many :rewards, dependent: :destroy
  has_many :donations, dependent: :destroy
  has_one_attached :picture
  
  has_many :articles, as: :parent, dependent: :destroy
  has_many :questions, as: :container, dependent: :destroy
  
  # --- VALIDATIONS ----------------------------
  
  validates :token,
        presence: true,
        uniqueness: true
  validates_presence_of :title
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> { where( status: 'published' )
        .where( 'published_at < ?', Time.zone.now ) }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    "#{token}-#{title.parameterize}"
  end
  
  def preview_text
    title
  end
  
  def goal_amount
    # check the funding progress
    goal_1
  end
  
  def goal_text
    #check the funding progress
    goal_1_use
  end
  
  def collected_amount
    donations.received.inject( 0 ) { |sum, d|
          sum + d.sum }
  end
  def collected_percentage
    case
      when goal_1.to_f <= 0
        100
      when collected_amount <= 0
        0
      when collected_amount >= goal_2.to_f
        collected_amount.to_f / goal_3.to_f * 100
      when collected_amount >= goal_1.to_f
        collected_amount.to_f / goal_2.to_f * 100
      else
        collected_amount.to_f / goal_1.to_f * 100
    end
  end
  
  def time_left_text
    now = Time.zone.now
    case
      when ends_at - now > 2.day
        "#{(( ends_at - now ) / 1.day).round} Tage"
      when ends_at - now > 2.hour
        "#{(( ends_at - now ) / 1.hour).round} Stunden"
      else
        "#{(( ends_at - now ) / 1.minute)} Minuten"
    end
  end
  
  def some_1_service
    SOME_SERVICES.detect { |s| some_1 =~ Regexp.new( s ) }
  end
  
  def some_2_service
    SOME_SERVICES.detect { |s| some_2 =~ Regexp.new( s ) }
  end
  
  def some_3_service
    SOME_SERVICES.detect { |s| some_3 =~ Regexp.new( s ) }
  end
  
  def booked( in_reward )
    return 'unbegrenzt' if in_reward.available.blank?
    in_reward.available - 11
  end
  
  def funders
    0
  end
  
  # --- Predicates -----------------------------
  
  def published?
    status == 'published'
  end
  
  def not_started?
    starts_at >= Time.zone.now
  end
  
  def has_picture?
    picture.attached?
  end
  
  # --- Picture Variants -----------------------
  
  def picture_article
    return '' unless has_picture?
    picture.variant( combine_options: { resize: '1200>', quality: '50' } )
  end
  
  def picture_preview
    return '' unless has_picture?
    picture.variant( combine_options: { gravity: 'Center', resize: '800x600^', crop: '800x600+0+0', quality: '50' } )  end
  
  def picture_twitter
    return '' unless has_picture?
    picture.variant( combine_options: { gravity: 'Center', resize: '1200x628^', crop: '1200x628+0+0', quality: '50' } )
  end
  
  # --- CONSTANTS ------------------------------
  
  SOME_SERVICES = %w( twitter facebook instagram tiktok mastodon )
  
end
