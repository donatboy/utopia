class Payment < ApplicationRecord
  attr_accessor :membership_token
  
  before_validation :lookup_membership
  
  belongs_to :membership
  
  # --- INSTANCE METHODS -----------------------
  
  def lookup_membership
    logger.debug "> lookup_membership @ payment"
    membership = Membership.find_by token: membership_token
    self.membership = membership
  end
  
end
