class Donation < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  belongs_to :funding
  belongs_to :contact
  accepts_nested_attributes_for :contact
  belongs_to :reward, optional: true
  
  # --- VALIDATIONS ----------------------------
  
  validates :token,
        presence: true,
        uniqueness: true
  validates_presence_of :status
  
  # --- SCOPES ---------------------------------
  
  scope :received, -> { where status: [ 'received', 'sent', 'closed' ] }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    token
  end
  
  def iid
    "do_#{token}"
  end
  
  def preview_text
    amount || reward.title
  end
  
  def sum
    result = amount.to_f
    result += reward.price || 0.0 unless reward.blank?
    return result
  end
  
  def next_status
    case status
      when 'new' then 'received'
      when 'received' then 'sent reward'
      else 'closed'
    end
  end
  
  def advance_status
    update_attribute :status, next_status
  end
  
  # --- Predicates -----------------------------
  
  def closed?
    status == 'closed'
  end
  
  # --- CONSTANTS ------------------------------
  
  STATUS_OPTIONS = [
    'new',
    'received',
    'sent reward',
    'closed'
  ]
  
end
