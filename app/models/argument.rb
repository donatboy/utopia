class Argument < ApplicationRecord
  belongs_to :account
  belongs_to :proposal
end
