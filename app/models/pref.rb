class Pref < ApplicationRecord
  
  def iid
    "prf_#{id}"
  end
  
  def active?
    case
      when is_boolean?
        value == 'true'
      when is_string?
        !value.blank?
    end
  end
  
  def is_boolean?
    klass == 'Boolean'
  end
  def is_string?
    klass == 'String'
  end
  
  def switch_value
    if value == 'true'
      self.value = 'false'
    else
      self.value = 'true'
    end
    return value
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.for_status?( in_status )
    status = Account.all_status_below( in_status )
    Pref.where( level: status ).size > 0
  end
  
end
