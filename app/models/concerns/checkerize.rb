module Checkerize
  extend ActiveSupport::Concern
  
  attr_accessor :op_a, :op_b, :solved
  
  def seed_checker_question
    self.op_a = rand 1..9
    self.op_b = rand 1..9
    logger.debug "-- seed_checker_question op_a: #{self.op_a}"
    logger.debug "-- seed_checker_question op_b: #{self.op_b}"
  end
  
  def check_question
    logger.debug "-- check_question op_a: #{op_a}"
    logger.debug "-- check_question op_b: #{op_b}"
    unless op_a.to_i > 0 and
        op_b.to_i > 0 and
        ( op_a.to_i + op_b.to_i == solved.to_i )
      errors.add( :solved, 'ist leider falsch' )
    end
  end
  
end
