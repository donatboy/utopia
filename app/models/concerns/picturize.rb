module Picturize
  extend ActiveSupport::Concern
  
  # --- PREDICATES -----------------------------
  
  def has_picture?
    picture.attached?
  end
  
  def picture_broken?
    picture_article.nil?
  end
  
  # --- INSTANCE METHODS -----------------------
  
  def file_name
    return '' unless has_picture?
    picture.filename
  end
  
  def file_size
    return '' unless has_picture?
    picture.byte_size
  end
  
  def picture_variant( in_selector, in_options = {} )
    logger.debug "> picture_variant @ picturize - in_selector:#{in_selector} - in_options:#{in_options}"
    logger.debug "-- @picture_variant:#{@picture_variant}"
    unless defined? @picture_variant
      @picture_variant = {}
    end
    unless @picture_variant.has_key? in_selector
      @picture_variant[ in_selector ] = nil
      
      if has_picture?
        begin
          @picture_variant[ in_selector ] = picture.variant( combine_options: in_options )
          
        rescue
          # nothing
          logger.debug "-- had to be rescued..."
        end
      end
    end
    
    return @picture_variant[ in_selector ]
  end
  
  def picture_thumb
    picture_variant :thumb, gravity: 'Center', resize: '300x300^', crop: '300x300+0+0', quality: '40'
  end
  
  def picture_preview
    picture_variant :preview, gravity: 'Center', resize: '800x800^', crop: '800x800+0+0', quality: '50'
  end
  
  def picture_article
    logger.debug "> picture_article @ picturize"
    picture_variant :article, resize: '1200>', quality: '50'
  end
  
  def picture_twitter
    picture_variant :twitter, auto_orient: '', gravity: 'Center', resize: '1200x628^', crop: '1200x628+0+0', quality: '50'
  end
  
end
