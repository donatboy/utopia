class Vote < ApplicationRecord
  belongs_to :account
  belongs_to :proposal
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> { where status: 'published' }
  scope :for_account, -> ( a ) {
        where account: a }
  scope :for_decision, -> ( d ) {
        select { |v|
            v.proposal.decision == d } }
  
  # --- INSTACE METHODS ------------------------
  
  def iid
    "vo_#{id}"
  end
  
  def published?
    status == 'published'
  end
  
end
