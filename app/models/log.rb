class Log < ApplicationRecord
  belongs_to :actor, polymorphic: true, optional: true
  belongs_to :entity, polymorphic: true, optional: true
  
  scope :for_cockpit, -> {
        where( 'created_at > ?', Time.zone.now - 2.days )
        .order( 'created_at DESC' )
        .reject { |l| l.ip.blank? }
        .reject { |l| l.from_bot? } }
  scope :for_cockpit_refresh, -> {
        where( 'created_at > ?', Time.zone.now - 3.hour - Time.zone.now.min.minutes - Time.zone.now.sec.seconds )
        .order( 'created_at DESC' )
        .reject { |l| l.ip.blank? }
        .reject { |l| l.from_bot? } }
  
  # --- INSTANCE METHODS -----------------------
  
  def from_bot?
    user_agent =~ BOT_SIGNATURES
  end
  
  # --- CLASS METHODS --------------------------
  
  # --- CONSTANTS ------------------------------
  
  BOT_SIGNATURES = /Datanyze|urllib|tweeted|PaperLi|ubermetricsTrendsmap|Twitterbot|curl|Go\-http|Mediatoolkitbot|check_http|DotBot|YandexBot|Baiduspider|Googlebot|bingbot|TurnitinBot|CareerBot|Ezooms|Exabot|oBot|SiteExplorer|Sosospider|MJ12bot|NetcraftSurveyAgent|Infohelfer|AhrefsBot|SISTRIX Crawler|Statsbot|CMS Crawler|Netcraft Web Server Survey|Snipebot|betaBot|spbot|Yahoo\! Slurp|AppEngine-Google|Java\/1\.\d\.|DuckDuckGo|nutch\-1|SEOkicks\-Robot/
end
