class Message < ApplicationRecord
  attr_accessor :to_members, :to_applicants,
        :to_helpers
  
  belongs_to :sender, polymorphic: true
  belongs_to :receiver, polymorphic: true, optional: true
  
  # --- VALIDATIONS ----------------------------
  
  validates_presence_of :content
  
  # --- INSTANCE METHODS -----------------------
  
  def to_members?
    @to_members.to_s == '1' or @to_members == true
  end
  
  def to_applicants?
    @to_applicants.to_s == '1' or @to_applicants == true
  end
  
  def to_helpers?
    @to_helpers.to_s == '1' or @to_helpers == true
  end
  
end
