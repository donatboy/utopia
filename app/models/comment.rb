class Comment < ApplicationRecord
  belongs_to :account
  belongs_to :entity, polymorphic: true
end
