module AnswersHelper
  
  def h_answer_editable?( in_answer )
    current_account and in_answer.contact == current_account.contact
  end
  
end
