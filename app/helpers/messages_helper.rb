module MessagesHelper
  
  def h_receiver_list
    @sent_messages.collect { |m| m.receiver.first_name }.join ', '
  end
  
end
