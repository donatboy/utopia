module QuestionsHelper
  
  def h_question_editable?( in_question )
    current_account and in_question.contact == current_account.contact
  end
  
end
