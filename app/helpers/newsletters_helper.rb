module NewslettersHelper
  
  def h_newsletter_editable?
    logger.debug "> h_newsletter_editable? @ NewslettersHelper -- current_account: #{current_account.to_yaml}"
    h_is_editor?
  end
  
end
