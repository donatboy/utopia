module FundingsHelper
  
  def h_funding_editable?
    logger.debug "> h_funding_editable? @ FundingsHelper -- current_account: #{current_account.to_yaml}"
    h_is_editor? and @funding.not_started?
  end
  
  def h_can_publish_funding?
    logger.debug "> h_can_publish_funding? @ FundingsHelper -- current_account: #{current_account.to_yaml}"
    !@funding.published? and h_is_editor? and current_account != @funding.account
  end
  
  def h_funding_actions?
    h_funding_editable? or h_can_publish_funding?
  end
  
end
