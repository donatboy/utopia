module ArticlesHelper
  
  def h_own_article?
    current_account and current_account == @article.author
  end
  
  def h_article_editable?
    #logger.debug "> h_article_editable? @ ArticlesHelper -- current_account: #{current_account.to_yaml}"
    h_is_editor? or h_own_article?
  end
  
  def h_can_publish_article?
    #logger.debug "> h_can_publish_article? @ ArticlesHelper -- current_account: #{current_account.to_yaml}"
    !@article.published? and
        ( ( h_is_team? and current_account != @article.author ) or
        Account.only_one_team? )
  end
  
end
