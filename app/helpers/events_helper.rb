module EventsHelper
  
  def h_event_editable?
    logger.debug "> h_event_editable? @ EventsHelper -- current_account: #{current_account.to_yaml}"
    h_is_editor?
  end
  
end