class AccountsController < ApplicationController
  include AccountHelper
  
  skip_before_action :require_team, only: [ :check, :request_password, :reset_password, :index, :show, :new, :edit, :create, :update ]
  before_action :set_account, only: [ :reset_password, :show, :edit, :update, :destroy ]
  
  # --- CUSTOM METHODS -------------------------
  
  # GET  /accounts/check
  # POST /accounts/check
  def check
    if request.post?
      @contact = Contact.find_by email: params[ :email ]
      
      if @contact.blank?
        redirect_to new_account_path( email: params[ :email ] ) and return
        
      elsif @contact.account.blank?
        token = @contact.generate_token
        @account = @contact.create_account(
            status: 'new',
            login: @contact.email,
            password: token,
            password_confirmation: token )
      end
      @contact.account.send_password_reset
      
      write_log 'check_account', params[ :email ], @contact
      render :account_sent
    end
  end
  
  def request_password
    if request.post?
      @contact = Contact.find_by email: params[ :email ]
      logger.debug "-- @account:#{@contact.to_yaml}"
      
      unless @contact and @contact.account
        redirect_to login_path, notice: 'E-Mail konnte nicht identifiziert werden' and return
        
      else
        @contact.account.send_password_reset
      end
      
      write_log 'request_password', params[ :email ], @contact
      render :password_sent
    end
  end
  
  def reset_password
    logger.debug "> reset_password @ account -- @account:#{@account.to_yaml} params[ :to ]:#{params[ :to ]}"
    
    unless request.patch?
      if @account.blank? or params[ :to ].blank? or @account.reset_token != params[ :to ]
        write_log 'reset_password', 'incorrect!', @account
        redirect_to root_path, alert: 'Nicht erlaubt!' and return
      end
      if @account.reset_expires_at <= Time.zone.now - RESET_PASSWORD_LINK_VALID_SPAN
        write_log 'reset_password', 'expired!', @account
        redirect_to login_path, alert: 'Link ist abgelaufen!' and return
      end
      
    else
      @account.status = 'confirmed' if @account.status == 'new'
      @account.contact.status = 'confirmed' if @account.contact.status = 'new'
      
      if @account.update( password: params[ :account ][ :password ], password_confirmation: params[ :account ][ :password_confirmation ], reset_token: nil )
        write_log 'reset_password', 'successful', @account
        redirect_to login_path, notice: 'Password wurde neu gesetzt, bitte anmelden.'
        
      else
        render
      end
    end
  end
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /accounts
  # GET /accounts.json
  def index
    render 'pages/no_access' and return unless h_page_accessible? 'accounts', current_account
    
    @accounts = Account.with_infos.published.shuffle
    
    write_log 'index_accounts'
  end
  
  # GET /accounts/1
  # GET /accounts/1.json
  def show
    redirect_to accounts_path, alert: "Nicht freigegeben!" and return unless @account.published? or @account == current_account
  end
  
  # GET /accounts/new
  def new
    redirect_to check_accounts_path unless params[ :email ] and params[ :email ] =~ URI::MailTo::EMAIL_REGEXP
    @account = Account.new( login: params[ :email ], contact: Contact.new )
    @account.contact.email = params[ :email ]
    @account.contact.seed_checker_question
  end
  
  # GET /accounts/1/edit
  def edit
    redirect_to login_path,
            notice: 'Bitte anmelden!' and return unless h_account_editable?( @account )
  end
  
  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new( account_params )
    @account.status = 'new'
    @account.contact.status = 'new' if @account.contact
    
    respond_to do |format|
      if @account.save
        ContactsMailer.with( contact: @account.contact ).
            confirmation_mail.
            deliver_later
        
        set_current_account @account
        
        format.html { redirect_to @account, notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @account }
      else
        @account.contact.seed_checker_question
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    redirect_to login_path,
            notice: 'Bitte anmelden!' and return unless h_account_editable?( @account )
    
    the_params = account_params
    # if passwort will be set, check old_password
    unless the_params[ :password ].blank?
      the_params[ :old_password ] = 'wrong!' unless @account.authenticate account_params[ :old_password ]
    end
    
    respond_to do |format|
      if @account.update( the_params )
        format.html { redirect_to @account, notice: 'Account wurde erfolgreich gespeichert.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_account
    @account = Account.find_by token: params[ :id ]
    render_404 and return if @account.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def account_params
    params.require( :account ).permit( :login, :password, :old_password, :password_confirmation, :displayname, :label, :description, :picture, :contact_attributes => [ :email, :first_name, :last_name, :is_member, :read_basics, :read_privacy, :wants_news, :op_a, :op_b, :solved ] )
  end
  
  # --- CONSTANTS ------------------------------
  
  RESET_PASSWORD_LINK_VALID_SPAN = 2.hours
  
end
