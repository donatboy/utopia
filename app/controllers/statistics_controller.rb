class StatisticsController < ApplicationController
  
  def index
  end
  
  def current_requests
    @today = Time.zone.now
    
    @requests_1 = Log.find_by_sql [ 'SELECT count(*) AS amount, entity_type, entity_id
        FROM logs
        WHERE created_at > :t1
        GROUP BY entity_type, entity_id
        ORDER by amount DESC', t1: @today - 24.hours ]
    @requests_2 = Log.find_by_sql [ 'SELECT count(*) AS amount, entity_type, entity_id
        FROM logs
        WHERE created_at > :t1 AND created_at <= :t2
        GROUP BY entity_type, entity_id
        ORDER by amount DESC', t1: @today - 48.hours, t2: @today - 24.hours ]
    
    @requests_3 = Log.find_by_sql [ 'SELECT count(*) AS amount, entity_type, entity_id
        FROM logs
        WHERE created_at > :t1 AND created_at <= :t2
        GROUP BY entity_type, entity_id
        ORDER by amount DESC', t1: @today - 72.hours, t2: @today - 48.hours ]
    
    @requests_w = Log.find_by_sql [ 'SELECT count(*) AS amount, entity_type, entity_id
        FROM logs
        WHERE created_at > :t1 AND created_at <= :t2
        GROUP BY entity_type, entity_id
        ORDER by amount DESC', t1: @today - 72.hours - 1.week, t2: @today - 72.hours ]
    
    @entities = @requests_1.
        collect { |r| r.entity }.
        compact
    @entities.reject! { |e| e.kind_of? Contact }
    d0 = @requests_1.detect { |r| r.entity == @entities.first }
    d1 = @requests_2.detect { |r| r.entity == @entities.first }
    d2 = @requests_3.detect { |r| r.entity == @entities.first }
    w = @requests_w.detect { |r| r.entity == @entities.first }
    @requests = @entities.collect { |e| [
          @requests_1.detect { |r| r.entity == e },
          @requests_2.detect { |r| r.entity == e },
          @requests_3.detect { |r| r.entity == e },
          @requests_w.detect { |r| r.entity == e } ].compact }
  end
  
end
