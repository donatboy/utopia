class AnswersController < ApplicationController
  before_action :set_answer, only: [ :show, :edit, :update, :destroy ]
  before_action :set_question, only: [ :new, :create ]
  
  skip_before_action :require_team
  before_action :require_editor, except: [ :index, :show ]
  
  # --------------------------------------------
  # CRUD methods
  
  # GET /answers/new
  def new
    @answer = @question.answers.build status: 'new'
  end
  
  # GET /answers/1/edit
  def edit
  end
  
  # POST /answers
  # POST /answers.json
  def create
    @answer = @question.answers.build( answer_params  )
    @answer.status = 'new'
    @answer.contact = current_account.contact
    @answer.openness = 'public'
    
    respond_to do |format|
      if @answer.save
        format.html { redirect_to @answer.question, notice: 'Antwort wurde erfolgreich gespeichert.' }
        format.json { render :show, status: :created, location: @answer }
      else
        format.html { render :new }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /answers/1
  # PATCH/PUT /answers/1.json
  def update
    respond_to do |format|
      if @answer.update( answer_params )
        format.html { redirect_to @answer.question, notice: 'Antwort wurde erfolgreich geändert.' }
        format.json { render :show, status: :ok, location: @answer }
      else
        format.html { render :edit }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /answers/1
  # DELETE /answers/1.json
  def destroy
    @question = @answer.question
    @answer.destroy
    
    respond_to do |format|
      format.html { redirect_to @question, notice: 'Antwort wurde gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_answer
    @answer = Answer.find_by token: params[ :id ].split( '-' ).first
    render_404 and return if @answer.blank?
  end
  
  def set_question
    redirect_to questions_path, alert: 'Antwort kann nur zu einer Frage erstellt werden.' and return unless params[ :question_id ]
    
    if numeric? params[ :question_id ]
      @question = Question.find params[ :question_id ].to_i
    else
      @question = Question.find_by token: params[ :question_id ].split( '-' ).first
    end
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def answer_params
    params.require( :answer ).permit( :status, :content )
  end
  
end
