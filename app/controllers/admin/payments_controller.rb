class Admin::PaymentsController < ApplicationController
  include PaymentsHelper
  
  before_action :require_admin
  
  before_action :check_enabled
  before_action :set_payment, only: [ :show, :edit, :update, :destroy ]
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /accounts
  # GET /accounts.json
  def index
    today = Time.zone.now.at_beginning_of_day
    @payment = Payment.new( { paid_at: today - 1.day } )
    @payments = Payment.order 'paid_at DESC'
  end
  
  # POST /accounts
  # POST /accounts.json
  def create
    @payment = Payment.new( payment_params )
    @payment.status = 'new'
    
    respond_to do |format|
      if @payment.save
        @payment.membership.advance_fee_check
        
        format.html { redirect_to admin_payments_path, notice: 'Zahlung wurde eingetragen.' }
        format.json { render :show, status: :created, location: @payment }
      else
        format.html { render :new }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def check_enabled
    render :not_enabled and return unless Pref.find_by( key: :allow_payment ).value == 'true'
  end
  
  def set_payment
    @payment = Payment.find params[ :id ]
    render_404 and return if @payment.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def payment_params
    params.require( :payment ).permit( :status, 
    :membership_token, :amount, :paid_at )
  end
  
end
