json.extract! page, :id, :status, :parent, :, :path, :title, :menu, :description, :created_at, :updated_at
json.url page_url(page, format: :json)
