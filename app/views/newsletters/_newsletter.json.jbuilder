json.extract! newsletter, :id, :token, :status, :content, :sent_at, :created_at, :updated_at
json.url newsletter_url(newsletter, format: :json)
