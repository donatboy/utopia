json.extract! decision, :id, :token, :status, :title, :account_id, :description, :end_proposals_at, :end_voting_at, :created_at, :updated_at
json.url decision_url(decision, format: :json)
