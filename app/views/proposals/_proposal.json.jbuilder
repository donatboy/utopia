json.extract! proposal, :id, :token, :status, :account_id, :decision_id, :title, :description, :created_at, :updated_at
json.url proposal_url(proposal, format: :json)
