json.extract! stream, :id, :token, :status, :title, :owner_id, :public, :max_participants, :only_invites, :status_required, :fade_away_days, :created_at, :updated_at
json.url stream_url(stream, format: :json)
