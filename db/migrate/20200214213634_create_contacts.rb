class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :token
      t.string :status
      t.string :email
      t.string :first_name
      t.string :last_name
      t.boolean :read_privacy
      t.boolean :wants_news
      t.datetime :news_last_sent
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
