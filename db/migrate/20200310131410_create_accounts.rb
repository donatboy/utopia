class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :status
      t.string :login
      t.string :password_digest
      t.references :contact, foreign_key: true
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
