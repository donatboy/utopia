class AddRegistrationStartToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :registration_starts_at, :datetime
    add_column :events, :registration_ends_at, :datetime
    add_column :events, :max_registrations, :integer
  end
end
