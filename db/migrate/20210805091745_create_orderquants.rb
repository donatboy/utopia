class CreateOrderquants < ActiveRecord::Migration[5.2]
  def change
    create_table :orderquants do |t|
      t.references :order, foreign_key: true
      t.references :reward, foreign_key: true
      t.integer :quantity
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
