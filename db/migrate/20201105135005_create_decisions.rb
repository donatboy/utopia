class CreateDecisions < ActiveRecord::Migration[5.2]
  def change
    create_table :decisions do |t|
      t.string :token
      t.string :status
      t.string :title
      t.references :account, foreign_key: true
      t.text :description
      t.datetime :end_proposals_at
      t.datetime :end_voting_at

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
