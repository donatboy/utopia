class AddAccountToMembership < ActiveRecord::Migration[5.2]
  def change
    add_column :memberships, :account_owner, :string
    add_column :memberships, :iban, :string
    add_column :memberships, :accepts_debit, :boolean
  end
end
