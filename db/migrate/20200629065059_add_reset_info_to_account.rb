class AddResetInfoToAccount < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :reset_token, :string
    add_column :accounts, :reset_expires_at, :datetime
  end
end
