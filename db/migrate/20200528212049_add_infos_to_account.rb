class AddInfosToAccount < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :label, :string
    add_column :accounts, :description, :text
  end
end
