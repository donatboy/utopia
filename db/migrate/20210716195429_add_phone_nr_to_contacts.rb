class AddPhoneNrToContacts < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :phone_nr, :string
  end
end
