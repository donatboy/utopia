class AddAddressToContacts < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :street, :string
    add_column :contacts, :housenr, :string
    add_column :contacts, :city, :string
    add_column :contacts, :wants_form, :boolean
  end
end
