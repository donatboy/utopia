class AddRequestsToRegistration < ActiveRecord::Migration[5.2]
  def change
    add_column :registrations, :requests, :string
  end
end
