class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.string :token
      t.string :status
      t.string :category
      t.references :container, polymorphic: true
      t.references :contact, foreign_key: true
      t.string :openness
      t.text :content

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
