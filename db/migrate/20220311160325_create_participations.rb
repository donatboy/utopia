class CreateParticipations < ActiveRecord::Migration[5.2]
  def change
    create_table :participations do |t|
      t.string :status
      t.references :entity, polymorphic: true
      t.references :participant, foreign_key: { to_table: :contacts }
      t.datetime :ends_at
      t.boolean :notify

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
