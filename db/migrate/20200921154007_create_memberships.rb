class CreateMemberships < ActiveRecord::Migration[5.2]
  def change
    create_table :memberships do |t|
      t.string :token
      t.string :status
      t.references :contact, foreign_key: true
      t.string :gender
      t.date :birth_on
      t.string :street
      t.string :housenr
      t.string :plz
      t.string :city
      t.text :other_memberships
      t.decimal :fee_amount, precision: 10, scale: 2
      t.string :fee_period
      t.string :fee_mode
      t.boolean :wants_membership
      t.datetime :starts_at
      t.datetime :ends_at
      t.datetime :last_payment_at
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
