class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :token
      t.string :status
      t.string :titel
      t.datetime :starts_at
      t.datetime :ends_at
      t.string :description
      t.string :location
      t.text :details
      t.string :url
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
