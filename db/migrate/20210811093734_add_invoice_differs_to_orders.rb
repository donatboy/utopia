class AddInvoiceDiffersToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :invoice_differs, :boolean
  end
end
