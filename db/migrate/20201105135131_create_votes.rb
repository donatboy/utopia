class CreateVotes < ActiveRecord::Migration[5.2]
  def change
    create_table :votes do |t|
      t.string :status
      t.references :account, foreign_key: true
      t.references :proposal, foreign_key: true
      t.integer :value

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
