class CreatePrefs < ActiveRecord::Migration[5.2]
  def change
    create_table :prefs do |t|
      t.string :status
      t.string :level
      t.string :key
      t.string :description
      t.string :value
      t.string :klass
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
