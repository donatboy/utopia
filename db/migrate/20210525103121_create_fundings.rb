class CreateFundings < ActiveRecord::Migration[5.2]
  def change
    create_table :fundings do |t|
      t.string :token
      t.string :status
      t.references :account
      t.string :title
      t.string :slogan
      t.text :short_text
      t.integer :goal_1
      t.integer :goal_2
      t.integer :goal_3
      t.string :goal_1_use
      t.string :goal_2_use
      t.string :goal_3_use
      t.datetime :starts_at
      t.datetime :ends_at
      t.string :performance_period
      t.text :description
      t.string :category
      t.string :location
      t.string :url
      t.string :some_1
      t.string :some_2
      t.string :some_3
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
