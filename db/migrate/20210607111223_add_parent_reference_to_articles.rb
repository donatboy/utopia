class AddParentReferenceToArticles < ActiveRecord::Migration[5.2]
  def change
    add_reference :articles, :parent, polymorphic: true
  end
end
