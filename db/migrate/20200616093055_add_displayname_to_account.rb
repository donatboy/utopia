class AddDisplaynameToAccount < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :displayname, :string
  end
end
