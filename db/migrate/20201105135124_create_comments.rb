class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.string :token
      t.string :status
      t.references :account, foreign_key: true
      t.references :entity, polymorphic: true
      t.text :content

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
