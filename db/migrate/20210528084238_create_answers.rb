class CreateAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :answers do |t|
      t.string :token
      t.string :status
      t.references :question, foreign_key: true
      t.references :contact, foreign_key: true
      t.string :openness
      t.text :content

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
