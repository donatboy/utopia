class AddShowResultsAtToDecision < ActiveRecord::Migration[5.2]
  def change
    add_column :decisions, :show_results_at, :datetime
  end
end
