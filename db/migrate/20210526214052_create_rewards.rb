class CreateRewards < ActiveRecord::Migration[5.2]
  def change
    create_table :rewards do |t|
      t.string :token
      t.string :status
      t.references :funding, foreign_key: true
      t.decimal :price, precision: 10, scale: 2
      t.string :title
      t.integer :available
      t.text :description
      t.string :delivery
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
