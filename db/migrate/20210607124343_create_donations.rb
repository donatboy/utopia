class CreateDonations < ActiveRecord::Migration[5.2]
  def change
    create_table :donations do |t|
      t.string :token
      t.string :status
      t.references :funding, foreign_key: true
      t.decimal :amount, precision: 10, scale: 2
      t.references :contact, foreign_key: true
      t.references :reward, foreign_key: true
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
