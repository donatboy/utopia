class AddRemarksToContacts < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :remarks, :text
  end
end
