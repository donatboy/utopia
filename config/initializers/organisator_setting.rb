# Be sure to restart your server when you modify this file.

# Naming of your organisation and her domain
Rails.application.config.orga_name = 'Utopia Community'
Rails.application.config.orga_domain = 'project.utopia-community.org'
Rails.application.config.orga_slogan = 'The new crypto force of 2022 for a better world'
Rails.application.config.orga_color = '#0a7b69'
